#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"

OUTPUT_FILE_NAME="clickquery.openapi.yaml"

rm -rf "${DIR}/bundle.tmp"
mkdir -p "${DIR}/bundle.tmp"
swagger-combine "${DIR}/main.yaml" > "${DIR}/bundle.tmp/${OUTPUT_FILE_NAME}"

if [[ -f "${DIR}/bundle.tmp/${OUTPUT_FILE_NAME}" ]] && [[ -f "${DIR}/bundle/${OUTPUT_FILE_NAME}" ]]; then
  difference=$(diff "${DIR}/bundle.tmp/${OUTPUT_FILE_NAME}" "${DIR}/bundle/${OUTPUT_FILE_NAME}")
  if [[ "${difference}" != "" ]]; then
    rm -rf "${DIR}/bundle/"
    mv "${DIR}/bundle.tmp/" "${DIR}/bundle/"
  fi
else
  mv "${DIR}/bundle.tmp/" "${DIR}/bundle/"
fi
