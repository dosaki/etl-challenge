# ETL
This is the Extraction/Transform/Load component of this tool.

It is meant to be run on a schedule (like cron, or a cloudwatch trigger).

**Note:** This was made in a manner that can be used as an AWS lambda function too.

## Pre-requisites
* Java 11
* Groovy 3
* Gradle 7.2
* MariaDB

## Building

```bash
./gradlew assemble
```

## Database set up

```bash
export DB_USERNAME=clicks
export DB_PASSWORD=SuperSecurePassword
export DB_HOST=localhost
export DB_PORT=3306
java -cp ./build/libs/etl-0.1-all.jar net.dosaki.etl.RequestHandler "seed"
```

or alternatively

```bash
export DB_ADMIN_USER=user          # CHANGE THIS!
export DB_ADMIN_PASSWORD=password  # CHANGE THIS!
mysql -u${DB_ADMIN_USER} -p${DB_ADMIN_PASSWORD} --execute="create database clicks;"
mysql -u${DB_ADMIN_USER} -p${DB_ADMIN_PASSWORD} clicks --execute="create table clicks.data_points (
                                       datasource  varchar(1024),
                                       campaign    varchar(1024),
                                       daily       date,
                                       clicks      integer,
                                       impressions integer,
                                       PRIMARY KEY (datasource, campaign, daily)
                                   );"
mysql -u${DB_ADMIN_USER} -p${DB_ADMIN_PASSWORD} --execute="create user clicks@'%' identified by 'SuperSecurePassword';grant all privileges  on clicks.* to clicks@'%';FLUSH PRIVILEGES;"
```

## Running

```bash
export DB_USERNAME=clicks
export DB_PASSWORD=SuperSecurePassword
export DB_HOST=localhost
export DB_PORT=3306
java -cp ./build/libs/etl-0.1-all.jar net.dosaki.etl.RequestHandler <url here>
```