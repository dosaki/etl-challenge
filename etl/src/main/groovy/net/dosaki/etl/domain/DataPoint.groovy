package net.dosaki.etl.domain

import java.sql.PreparedStatement

class DataPoint {
    String datasource
    String campaign
    Date daily
    Integer clicks
    Integer impressions

    static String preparedInsertQuery = "INSERT INTO data_points (datasource, campaign, daily, clicks, impressions) VALUES (?, ?, ?, ?, ?)" +
            "ON DUPLICATE KEY UPDATE clicks=VALUES(clicks),impressions=VALUES(impressions)"
    static String createTableQuery = """CREATE TABLE IF NOT EXISTS clicks.data_points (
                                       datasource  VARCHAR(255),
                                       campaign    VARCHAR(255),
                                       daily       DATE,
                                       clicks      INTEGER,
                                       impressions INTEGER,
                                       PRIMARY KEY (datasource, campaign, daily)
                                   );"""

    DataPoint(String datasource, String campaign, Date daily, Integer clicks, Integer impressions) {
        this.datasource = datasource
        this.campaign = campaign
        this.daily = daily
        this.clicks = clicks ?: 0
        this.impressions = impressions ?: 0
    }

    void addToStatement(PreparedStatement statement) {
        statement.setString(1, this.datasource)
        statement.setString(2, this.campaign)
        statement.setDate(3, new java.sql.Date(this.daily.time))
        statement.setInt(4, this.clicks)
        statement.setInt(5, this.impressions)
        statement.addBatch()
    }
}
