package net.dosaki.etl

import io.micronaut.core.annotation.Introspected
import io.micronaut.function.aws.MicronautRequestHandler
import net.dosaki.etl.extract.Extractor
import net.dosaki.etl.domain.DataPoint
import net.dosaki.etl.helpers.MariaDBConnection
import net.dosaki.etl.transform.Metadata
import java.sql.PreparedStatement

@Introspected
class RequestHandler extends MicronautRequestHandler<Map, Boolean> {
    static void main(String[] args) {
        if (args[0] == "seed") {
            MariaDBConnection.connection.createStatement().executeQuery(DataPoint.createTableQuery)
            return
        }
        Map input = ["url": args[0]]
        new RequestHandler().execute(input)
    }

    @Override
    Boolean execute(Map input) {
        println "Starting..."
        PreparedStatement statement = MariaDBConnection.connection.prepareStatement(DataPoint.preparedInsertQuery)
        List<DataPoint> dataPoints = input.url.with(Extractor.&extract >> Metadata.&make >> { it.rows.collect { new DataPoint(*it) } })
        println "Preparing statements..."
        dataPoints.eachWithIndex { DataPoint dataPoint, int i ->
            dataPoint.addToStatement(statement)
        }
        println "Dumping into the database"
        statement.executeBatch()
        println "Done!"
    }
}