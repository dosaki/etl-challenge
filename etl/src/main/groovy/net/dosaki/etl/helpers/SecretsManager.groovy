package net.dosaki.etl.helpers

import com.amazonaws.services.secretsmanager.*
import com.amazonaws.services.secretsmanager.model.*
import com.amazonaws.client.builder.AwsClientBuilder
import groovy.json.JsonSlurper


class SecretsManager {
    static Map getSecret(String secretName, String region) {
        String endpoint = "secretsmanager.${region}.amazonaws.com"

        AwsClientBuilder.EndpointConfiguration config = new AwsClientBuilder.EndpointConfiguration(endpoint, region)
        AWSSecretsManagerClientBuilder clientBuilder = AWSSecretsManagerClientBuilder.standard()
        clientBuilder.setEndpointConfiguration(config)
        AWSSecretsManager client = clientBuilder.build()

        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName)
                .withVersionStage("AWSCURRENT")
        try {
            GetSecretValueResult getSecretValueResult = client.getSecretValue(getSecretValueRequest)
            String secret = getSecretValueResult?.secretString ?: getSecretValueResult?.secretBinary?.toString() ?: null
            return new JsonSlurper().parseText(secret)
        } catch (ResourceNotFoundException ignored) {
            System.out.println("The requested secret " + secretName + " was not found")
        } catch (InvalidRequestException e) {
            System.out.println("The request was invalid due to: " + e.getMessage())
        } catch (InvalidParameterException e) {
            System.out.println("The request had invalid params: " + e.getMessage())
        }
        return null
    }
}
