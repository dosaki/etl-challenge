package net.dosaki.etl.helpers

import java.sql.Connection
import java.sql.DriverManager

class MariaDBConnection {
    private static Map fetchDBDetails() {
        if(System.getenv('DB_HOST') && System.getenv('DB_PORT') && System.getenv('DB_USERNAME') && System.getenv('DB_PASSWORD')){
            return [
                    "host": System.getenv('DB_HOST'),
                    "port": System.getenv('DB_PORT'),
                    "username": System.getenv('DB_USERNAME'),
                    "password": System.getenv('DB_PASSWORD')
            ]
        }
        return SecretsManager.getSecret("etl-test-db", "eu-west-1")
    }

    static Connection getConnection() {
        Map secret = fetchDBDetails()
        Class.forName("org.mariadb.jdbc.Driver")
        return DriverManager.getConnection(
                "jdbc:mariadb://${secret.host}:${secret.port}/clicks", "${secret.username}", "${secret.password}")
    }
}
