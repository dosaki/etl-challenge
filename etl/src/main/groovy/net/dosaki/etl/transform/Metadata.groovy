package net.dosaki.etl.transform

class Metadata {
    List<Column> columns
    List<List<Object>> rows

    Metadata(String headers, List<String> data) {
        List<String> splitHeaders = headers.split(',')
        List<List<String>> rows = data*.split(',')*.toList()
        List<List<String>> dataSamples = splitHeaders
                .shuffled()[0..Math.min(splitHeaders.size()-1, 50)] // this needs to be replaced with something else, don't want to shuffle 1 billion items just to grab 50
                .withIndex()
                .collect { _, int i -> rows*.get(i) }

        this.columns = splitHeaders
                .withIndex()
                .collect { String header, int i -> new Column(header, dataSamples[i]) }
        this.rows = rows.collect { row ->
            row.withIndex().collect { Object item, int i ->
                this.columns[i].dataType.newInstance(item)
            }
        }
    }

    static Metadata make(List<String> csv) {
        return new Metadata(csv[0], csv[1..-1]) // need some protection here to ensure there is some data to use
    }
}