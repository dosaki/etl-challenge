package net.dosaki.etl.transform

import java.lang.IllegalArgumentException

class Column {
    String name
    Class dataType
    
    Column(String name, List<String> dataSample){
        this.name = name
        
        //Here we figure out what data we're dealing with
        List<Class> convertedDataSample = dataSample.collect {
            try{
                new Date(it)
                return Date
            } catch(IllegalArgumentException ignored){
                return it.number ? Integer : String
            }
        }.unique()
        this.dataType = convertedDataSample.size() > 1 ? String : convertedDataSample[0]
    }
}