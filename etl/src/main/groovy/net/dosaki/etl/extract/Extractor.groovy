package net.dosaki.etl.extract

class Extractor {
    static List<String> extract(String url) {
        return url.toURL().readLines()
    }
}