package net.dosaki.etl.transform

import spock.lang.Specification

class MetadataSpec extends Specification {

    void "test Metadata"() {
        given:
        String headers = "Datasource,Campaign,Daily,Clicks,Impressions"
        String data = """Google Ads,Adventmarkt Touristik,11/12/19,7,22425
Google Ads,Adventmarkt Touristik,11/13/19,16,45452
Google Ads,Adventmarkt Touristik,11/14/19,147,80351
Google Ads,Adventmarkt Touristik,11/15/19,131,81906
Google Ads,Adventmarkt Touristik,11/16/19,85,43910
Google Ads,Adventmarkt Touristik,11/17/19,107,36361
Google Ads,Adventmarkt Touristik,11/18/19,93,56982
Google Ads,Adventmarkt Touristik,11/19/19,120,23424
Google Ads,Adventmarkt Touristik,11/20/19,161,27841
Google Ads,Adventmarkt Touristik,11/21/19,256,44322
Google Ads,Adventmarkt Touristik,11/22/19,140,39414
Google Ads,Adventmarkt Touristik,11/23/19,123,37374
Google Ads,Adventmarkt Touristik,11/24/19,111,33158
Google Ads,Adventmarkt Touristik,11/25/19,102,28528
Google Ads,Adventmarkt Touristik,11/26/19,145,29114
Google Ads,Adventmarkt Touristik,11/27/19,182,32518
Google Ads,Adventmarkt Touristik,11/28/19,185,30767
Google Ads,Adventmarkt Touristik,11/29/19,80,33036
Google Ads,Adventmarkt Touristik,11/30/19,75,23558
Google Ads,Adventmarkt Touristik,12/01/19,76,25436
Google Ads,Adventmarkt Touristik,12/02/19,82,26557
Google Ads,Adventmarkt Touristik,12/03/19,110,26327
Google Ads,Adventmarkt Touristik,12/04/19,180,29416
Google Ads,Adventmarkt Touristik,12/05/19,253,53365
Google Ads,Adventmarkt Touristik,12/06/19,96,37034
Google Ads,Adventmarkt Touristik,12/07/19,130,38373
Google Ads,Adventmarkt Touristik,12/08/19,256,59663
Google Ads,Adventmarkt Touristik,12/09/19,111,47990
Google Ads,Adventmarkt Touristik,12/10/19,130,54400
Google Ads,Adventmarkt Touristik,12/11/19,195,48059
Google Ads,Adventmarkt Touristik,12/12/19,188,40666
Google Ads,Adventmarkt Touristik,12/13/19,46,28322
Google Ads,Adventmarkt Touristik,12/14/19,38,29950
Google Ads,Adventmarkt Touristik,12/15/19,40,35563
Google Ads,Adventmarkt Touristik,12/16/19,53,41855
Google Ads,Adventmarkt Touristik,12/17/19,108,53673
Google Ads,Adventmarkt Touristik,12/18/19,229,53497
Google Ads,Adventmarkt Touristik,12/19/19,155,60063
Google Ads,Adventmarkt Touristik,12/20/19,22,52496"""

        when: 'resolving the metadata'
        Metadata metadata = new Metadata(headers, data.readLines())

        then: 'it should have 5 columns'
        metadata.columns.size() == 5
        and: 'each column should be using the correct type'
        metadata.columns[0].dataType == String
        metadata.columns[1].dataType == String
        metadata.columns[2].dataType == Date
        metadata.columns[3].dataType == Integer
        metadata.columns[4].dataType == Integer
        and: 'there should be 39 rows'
        metadata.rows.size() == 39
        and: 'each row should have the same number of items as there are columns'
        metadata.rows[0].size() == metadata.columns.size()
        and: 'each item in a row should be using the correct type'
        metadata.rows[0][0].class == String
        metadata.rows[0][1].class == String
        metadata.rows[0][2].class == Date
        metadata.rows[0][3].class == Integer
        metadata.rows[0][4].class == Integer
    }
}
