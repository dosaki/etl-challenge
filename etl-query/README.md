# ETL Click Query API

This is the API counterpart to [etl](../etl/). It expects that etl would've been run already.

You can find the documentation [here](https://app.swaggerhub.com/apis-docs/dosaki/clickquery/).

## Pre-requisites
* Java 11
* Groovy 3
* Gradle 7.2
* MariaDB

## Building

```bash
./gradlew assemble
```

## Database set up

See [etl's README.md](../etl/README.md) or alternatively

```bash
export DB_ADMIN_USER=user          # CHANGE THIS!
export DB_ADMIN_PASSWORD=password  # CHANGE THIS!
mysql -u${DB_ADMIN_USER} -p${DB_ADMIN_PASSWORD} --execute="create database clicks;"
mysql -u${DB_ADMIN_USER} -p${DB_ADMIN_PASSWORD} clicks --execute="create table data_points (
                                       datasource  varchar(1024),
                                       campaign    varchar(1024),
                                       daily       date,
                                       clicks      integer,
                                       impressions integer,
                                       PRIMARY KEY (datasource, campaign, daily)
                                   );"
mysql -u${DB_ADMIN_USER} -p${DB_ADMIN_PASSWORD} --execute="create user clicks@'%' identified by 'SuperSecurePassword';grant all privileges  on clicks.* to clicks@'%';FLUSH PRIVILEGES;"
```

## Running

```bash
export DB_USERNAME=clicks
export DB_PASSWORD=SuperSecurePassword
export DB_HOST=localhost
export DB_PORT=3306
java -jar ./build/libs/etl-query-0.1-all.jar
```