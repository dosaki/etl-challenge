package net.dosaki.etl

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import groovy.transform.CompileStatic
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.QueryValue
import net.dosaki.etl.exceptions.InvalidValueException
import net.dosaki.etl.helpers.MariaDBConnection
import net.dosaki.etl.helpers.QueryBuilder
import net.dosaki.etl.response.ResponseBody
import net.dosaki.etl.response.Result

import java.sql.ResultSet

@CompileStatic
@Controller("/query")
class QueryController {
    static List<String> VALID_DIMENSION_VALUES = ["datasource", "campaign", "daily"]
    @Get("/")
    @Produces(MediaType.APPLICATION_JSON)
    ResponseBody query(
            @QueryValue(value = "dimension") List<String> dimensions,
            @QueryValue(value = "whereDatasource") Optional<String> whereDatasource,
            @QueryValue(value = "whereCampaign") Optional<String> whereCampaign,
            @QueryValue(value = "whereDaily") Optional<String> whereDaily
    ) {
        if(!VALID_DIMENSION_VALUES.containsAll(dimensions)){
            throw new InvalidValueException((dimensions - VALID_DIMENSION_VALUES), VALID_DIMENSION_VALUES)
        }
        String query = QueryBuilder.apiParamsAsSelectQuery(dimensions, whereDatasource, whereCampaign, whereDaily)
        ResultSet resultSet = MariaDBConnection
                .connection
                .createStatement()
                .executeQuery(query)
        return new ResponseBody(
                message: "Ok",
                result: Result.fromResultSet(dimensions, resultSet)
        )
    }
}
