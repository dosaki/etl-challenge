package net.dosaki.etl

import io.micronaut.context.annotation.Requires
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Produces
import io.micronaut.http.server.exceptions.ExceptionHandler
import jakarta.inject.Singleton
import net.dosaki.etl.exceptions.ETLException
import net.dosaki.etl.response.ResponseBody

@Produces
@Singleton
@Requires(classes = [Exception.class, ExceptionHandler.class])
class ExceptionController implements ExceptionHandler<Exception, HttpResponse> {

    @Override
    HttpResponse handle(HttpRequest request, Exception exception) {
        if (exception instanceof ETLException && (exception as ETLException).statusCode == 400) {
            return HttpResponse.badRequest(new ResponseBody(message: exception.message))
        }
        return HttpResponse.serverError(new ResponseBody(message: exception.message))
    }
}