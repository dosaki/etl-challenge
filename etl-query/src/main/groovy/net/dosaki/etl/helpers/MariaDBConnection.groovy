package net.dosaki.etl.helpers

import net.dosaki.etl.exceptions.NoDatabaseDetailsException

import java.sql.Connection
import java.sql.DriverManager

class MariaDBConnection {
    private static Map fetchDBDetails() {
        if(!(System.getenv('DB_HOST') && System.getenv('DB_PORT') && System.getenv('DB_USERNAME') && System.getenv('DB_PASSWORD'))){
            throw new NoDatabaseDetailsException()
        }
        return [
            "host": System.getenv('DB_HOST'),
            "port": System.getenv('DB_PORT'),
            "username": System.getenv('DB_USERNAME'),
            "password": System.getenv('DB_PASSWORD')
        ]
    }

    static Connection getConnection() {
        Map details = fetchDBDetails()
        Class.forName("org.mariadb.jdbc.Driver")
        return DriverManager.getConnection("jdbc:mariadb://${details.host}:${details.port}/clicks", "${details.username}", "${details.password}")
    }
}
