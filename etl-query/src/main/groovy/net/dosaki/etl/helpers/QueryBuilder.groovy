package net.dosaki.etl.helpers

import net.dosaki.etl.domain.DataPoint
import net.dosaki.etl.exceptions.InvalidSyntaxException

class QueryBuilder {
    static String apiParamsAsSelectQuery(List<String> dimensions, Optional<String> whereDatasource, Optional<String> whereCampaign, Optional<String> whereDaily) {
        List<String> columns = dimensions - ["", null]
        if(columns.isEmpty()){
            throw new InvalidSyntaxException("List of dimensions requires at least one non-empty value")
        }
        String whereClause = new WhereStatementBuilder()
                .add("datasource", whereDatasource)
                .add("campaign", whereCampaign)
                .add("daily", whereDaily)
                .toString()
        return DataPoint.asSelectString(columns - [null], whereClause)
    }
}
