package net.dosaki.etl.helpers

import net.dosaki.etl.exceptions.InvalidSyntaxException
import net.dosaki.etl.exceptions.InvalidValueException

class WhereStatementBuilder {
    List<String> restrictions = []
    static Map<String, Closure> OPERATIONS = [
            "EQ"  : { String it -> "= '${it}'" },
            "NEQ" : { String it -> "!= '${it}'" },
            "IN"  : { String it -> "IN ('${it.split("::").join("', '")}')" },
            "NIN" : { String it -> "NOT ${OPERATIONS["IN"](it)}" },
            "BTW" : { String it ->
                String[] values = it.split("::")
                return "BETWEEN '${values[0]}' AND '${values[1]}'"
            },
            "NBTW": { String it -> "NOT ${OPERATIONS["BTW"](it)}" }
    ]

    private static String queryParamToSqlRestriction(String queryParam) {
        if (!queryParam || !queryParam.matches(/\[[A-Za-z]+]\(.*\)/)) {
            throw new InvalidSyntaxException("${queryParam} is malformed")
        }
        String[] splitParam = queryParam.split("]\\(")
        String operation = splitParam[0][1..-1].toUpperCase()
        if (!OPERATIONS.containsKey(operation)) {
            throw new InvalidValueException(operation, OPERATIONS.keySet().toList())
        }
        return OPERATIONS[operation](splitParam[1][0..-2])
    }

    WhereStatementBuilder addString(String column, String queryParam) {
        if (queryParam && column) {
            this.restrictions.add("${column} ${queryParamToSqlRestriction(queryParam)}")
        }
        return this
    }

    WhereStatementBuilder add(String column, Optional<String> queryParam) {
        if (!queryParam.isEmpty()) {
            return this.addString(column, queryParam.get())
        }
        return this
    }

    String toString() {
        if (this.restrictions.isEmpty()) {
            return ""
        }
        return "WHERE ${this.restrictions.join(" AND ")}"
    }
}
