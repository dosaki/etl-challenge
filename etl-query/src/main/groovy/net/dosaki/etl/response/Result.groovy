package net.dosaki.etl.response


import java.sql.ResultSet

class Result {
    String datasource
    String campaign
    Date daily
    Metrics total
    Metrics average
    Metrics maximum
    Metrics minimum

    Result(String datasource, String campaign, Date daily,
           Integer totalClicks, Integer totalImpressions, float totalClickthrough,
           Integer averageClicks, Integer averageImpressions, float averageClickthrough,
           Integer maxClicks, Integer maxImpressions, float maxClickthrough,
           Integer minClicks, Integer minImpressions, float minClickthrough) {
        this.datasource = datasource
        this.campaign = campaign
        this.daily = daily
        this.total = new Metrics(clicks: totalClicks, impressions: totalImpressions, clickthrough: totalClickthrough)
        this.average = new Metrics(clicks: averageClicks, impressions: averageImpressions, clickthrough: averageClickthrough)
        this.maximum = new Metrics(clicks: maxClicks, impressions: maxImpressions, clickthrough: maxClickthrough)
        this.minimum = new Metrics(clicks: minClicks, impressions: minImpressions, clickthrough: minClickthrough)
    }

    static List<Result> fromResultSet(List<String> dimensions, ResultSet resultSet) {
        List<Result> results = []
        while(resultSet.next()){
            Map<String, Object> dimensionMap = dimensions.collectEntries{[(it):null]}
            dimensions.eachWithIndex { it, i ->
                dimensionMap[it] = it == "daily" ? resultSet.getDate(i+1) : resultSet.getString(i+1)
            }
            Integer offset = dimensions.size() + 1
            results.add(new Result(
                    dimensionMap["datasource"] as String, dimensionMap["campaign"] as String, dimensionMap["daily"] as Date,
                    resultSet.getInt(offset), resultSet.getInt(offset+1), resultSet.getFloat(offset+2),
                    resultSet.getInt(offset+3), resultSet.getInt(offset+4), resultSet.getFloat(offset+5),
                    resultSet.getInt(offset+6), resultSet.getInt(offset+7), resultSet.getFloat(offset+8),
                    resultSet.getInt(offset+9), resultSet.getInt(offset+10), resultSet.getFloat(offset+11),
            ))
        }
        return results
    }
}
