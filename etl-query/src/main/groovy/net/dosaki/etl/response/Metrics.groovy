package net.dosaki.etl.response

class Metrics {
    Integer clicks
    Integer impressions
    Float clickthrough
}
