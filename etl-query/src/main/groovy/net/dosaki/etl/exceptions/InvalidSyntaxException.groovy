package net.dosaki.etl.exceptions

class InvalidSyntaxException extends ETLException {
    String message = "Invalid syntax. See https://app.swaggerhub.com/apis-docs/dosaki/clickquery"
    InvalidSyntaxException(String problem) {
        if(problem){
            this.message = "Invalid syntax: ${problem}. See https://app.swaggerhub.com/apis-docs/dosaki/clickquery"
        }
    }
}
