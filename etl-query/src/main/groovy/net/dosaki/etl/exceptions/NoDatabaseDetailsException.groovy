package net.dosaki.etl.exceptions

class NoDatabaseDetailsException extends ETLException {
    Integer statusCode = 500
    String message = "No database details found. This is definitely our fault. Sorry!"
}
