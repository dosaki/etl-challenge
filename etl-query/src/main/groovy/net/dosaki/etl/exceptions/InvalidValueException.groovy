package net.dosaki.etl.exceptions

class InvalidValueException extends ETLException {
    String message = "Unacceptable value(s). See https://app.swaggerhub.com/apis-docs/dosaki/clickquery"

    private static generateMessage(List<String> values, List<String> validValues) {
        return "Unacceptable values: ${values.join(', ')}. Allowed values are ${validValues.join(', ')}. See https://app.swaggerhub.com/apis-docs/dosaki/clickquery"
    }

    InvalidValueException(List<String> values, List<String> validValues) {
        if(values){
            this.message = generateMessage(values, validValues)
        }
    }
    InvalidValueException(String value, List<String> validValues) {
        if(value){
            this.message = generateMessage([value], validValues)
        }
    }
}
