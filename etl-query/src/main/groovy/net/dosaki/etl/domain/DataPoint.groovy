package net.dosaki.etl.domain

import java.sql.PreparedStatement

class DataPoint {
    String datasource
    String campaign
    Date daily
    Integer clicks
    Integer impressions

    DataPoint(String datasource, String campaign, Date daily, Integer clicks, Integer impressions) {
        this.datasource = datasource
        this.campaign = campaign
        this.daily = daily
        this.clicks = clicks?:0
        this.impressions = impressions?:0
    }

    static AVAILABLE_DIMENSIONS = ["datasource", "campaign", "daily"]
    
    static asSelectString(List<String> dimensions, String filters) {
        List<String> columns = dimensions.isEmpty() ? AVAILABLE_DIMENSIONS : dimensions
        return """SELECT
    ${columns.join(',')},
    SUM(clicks), SUM(impressions), SUM(clicks/impressions),
    AVG(clicks), AVG(impressions), AVG(clicks/impressions),
    MAX(clicks), MAX(impressions), MAX(clicks/impressions),
    MIN(clicks), MIN(impressions), MIN(clicks/impressions)
FROM data_points
${filters}
GROUP BY ${columns.join(',')};"""
    }

    void addToStatement(PreparedStatement statement) {
        statement.setString(1, this.datasource)
        statement.setString(2, this.campaign)
        statement.setDate(3, new java.sql.Date(this.daily.time))
        statement.setInt(4, this.clicks)
        statement.setInt(5, this.impressions)
        statement.addBatch()
    }
}
