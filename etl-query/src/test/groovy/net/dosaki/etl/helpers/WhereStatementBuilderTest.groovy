package net.dosaki.etl.helpers

import net.dosaki.etl.exceptions.InvalidSyntaxException
import net.dosaki.etl.exceptions.InvalidValueException
import spock.lang.Specification

class WhereStatementBuilderTest extends Specification {
    def "queryParamToSqlRestriction"() {
        expect:
        WhereStatementBuilder.queryParamToSqlRestriction(input) == result

        where:
        input                                             | result
        "[EQ](something)"                                 | "= 'something'"
        "[eq](4)"                                         | "= '4'"
        "[NEQ](something else)"                           | "!= 'something else'"
        "[NeQ](4 with a string)"                          | "!= '4 with a string'"
        "[In](one thing::another thing::and another)"     | "IN ('one thing', 'another thing', 'and another')"
        "[NIN](one thing::another thing::and another)"    | "NOT IN ('one thing', 'another thing', 'and another')"
        "[BTW](one thing::another thing)"                 | "BETWEEN 'one thing' AND 'another thing'"
        "[NBTW](one thing::another thing)"                | "NOT BETWEEN 'one thing' AND 'another thing'"
        "[nbtw](one thing::another thing::ignored thing)" | "NOT BETWEEN 'one thing' AND 'another thing'"
    }

    def "queryParamToSqlRestriction - invalid operation"() {
        when:
        WhereStatementBuilder.queryParamToSqlRestriction("[BADOP](thing)")
        then:
        thrown InvalidValueException
    }

    def "queryParamToSqlRestriction - invalid string"() {
        when:
        WhereStatementBuilder.queryParamToSqlRestriction("hello")
        then:
        thrown InvalidSyntaxException
    }

    def "queryParamToSqlRestriction - null"() {
        when:
        WhereStatementBuilder.queryParamToSqlRestriction(null)
        then:
        thrown InvalidSyntaxException
    }

    def "toString when empty"() {
        given:
        WhereStatementBuilder where = new WhereStatementBuilder()
        when:
        String whereClause = where.toString()
        then:
        whereClause == ""
    }

    def "addString"() {
        given:
        WhereStatementBuilder where = new WhereStatementBuilder()
                .addString("col1", "[neq](thing)")
                .addString(column, item)
                .addString("col2", "[eq](4)")
        expect:
        where.restrictions.size() == size
        where.restrictions[1] == clause

        where:
        column | item       | clause        | size
        "col1" | "[eq](hi)" | "col1 = 'hi'" | 3
        "col2" | "[eq](5)"  | "col2 = '5'"  | 3
        null   | "[eq](5)"  | "col2 = '4'"  | 2
        ""     | "[eq](5)"  | "col2 = '4'"  | 2
        "col3" | ""         | "col2 = '4'"  | 2
        "col3" | null       | "col2 = '4'"  | 2
        null   | null       | "col2 = '4'"  | 2
        ""     | ""         | "col2 = '4'"  | 2
    }

    def "add"() {
        given:
        WhereStatementBuilder where = new WhereStatementBuilder()
                .add("col1", Optional<String>.of("[neq](thing)"))
                .add(column, item)
                .add("col2", Optional<String>.of("[eq](4)"))
        expect:
        where.restrictions.size() == size
        where.restrictions[1] == clause

        where:
        column | item                            | clause        | size
        "col1" | Optional<String>.of("[eq](hi)") | "col1 = 'hi'" | 3
        "col2" | Optional<String>.of("[eq](5)")  | "col2 = '5'"  | 3
        null   | Optional<String>.of("[eq](5)")  | "col2 = '4'"  | 2
        ""     | Optional<String>.of("[eq](5)")  | "col2 = '4'"  | 2
        "col3" | Optional<String>.empty()        | "col2 = '4'"  | 2
        "col3" | Optional<String>.of("")         | "col2 = '4'"  | 2
        null   | Optional<String>.empty()        | "col2 = '4'"  | 2
        ""     | Optional<String>.empty()        | "col2 = '4'"  | 2
    }

    def "toString when it has stuff"() {
        given:
        WhereStatementBuilder where = new WhereStatementBuilder()
                .addString("column1", "[IN](one thing::another thing::and another)")
                .addString("column2", "[EQ](something)")
                .addString("column1", "[NEQ](something else)")
                .addString("column3", "[BTW](one thing::another thing)")
        when:
        String whereClause = where.toString()
        then:
        whereClause == "WHERE column1 IN ('one thing', 'another thing', 'and another') AND column2 = 'something' AND column1 != 'something else' AND column3 BETWEEN 'one thing' AND 'another thing'"
    }
}
