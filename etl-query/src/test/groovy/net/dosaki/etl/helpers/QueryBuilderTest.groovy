package net.dosaki.etl.helpers

import net.dosaki.etl.exceptions.InvalidSyntaxException
import spock.lang.Specification

class QueryBuilderTest extends Specification {
    def "apiParamsAsSelectQuery"() {
        expect:
        QueryBuilder.apiParamsAsSelectQuery(dimensions, Optional.ofNullable(whereDatasource), Optional.ofNullable(whereCampaign), Optional.ofNullable(whereDaily)) == result

        where:
        dimensions      | whereDatasource    | whereCampaign    | whereDaily    | result
        ["a", "b", "c"] | "[eq](datasource)" | "[eq](campaign)" | "[eq](daily)" | "SELECT\n    a,b,c,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE datasource = 'datasource' AND campaign = 'campaign' AND daily = 'daily'\nGROUP BY a,b,c;"
        ["a", "b", "c"] | ""                 | ""               | ""            | "SELECT\n    a,b,c,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\n\nGROUP BY a,b,c;"
        ["a", "b", "c"] | null               | null             | null          | "SELECT\n    a,b,c,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\n\nGROUP BY a,b,c;"
        ["a", "b", "c"] | "[eq](datasource)" | ""               | ""            | "SELECT\n    a,b,c,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE datasource = 'datasource'\nGROUP BY a,b,c;"
        ["a"]           | "[eq](datasource)" | null             | null          | "SELECT\n    a,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE datasource = 'datasource'\nGROUP BY a;"
        ["a", null]     | ""                 | "[eq](campaign)" | ""            | "SELECT\n    a,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE campaign = 'campaign'\nGROUP BY a;"
        ["a", ""]       | null               | "[eq](campaign)" | null          | "SELECT\n    a,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE campaign = 'campaign'\nGROUP BY a;"
        ["a", null, ""] | ""                 | ""               | "[eq](daily)" | "SELECT\n    a,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE daily = 'daily'\nGROUP BY a;"
        ["a", null, ""] | null               | null             | "[eq](daily)" | "SELECT\n    a,\n    SUM(clicks), SUM(impressions), SUM(clicks/impressions),\n    AVG(clicks), AVG(impressions), AVG(clicks/impressions),\n    MAX(clicks), MAX(impressions), MAX(clicks/impressions),\n    MIN(clicks), MIN(impressions), MIN(clicks/impressions)\nFROM data_points\nWHERE daily = 'daily'\nGROUP BY a;"
    }

    def "apiParamsAsSelectQuery - empty dimensions"() {
        when:
        QueryBuilder.apiParamsAsSelectQuery([], Optional.ofNullable("[eq](datasource)"), Optional.ofNullable("[eq](campaign)"), Optional.ofNullable("[eq](daily)"))

        then:
        thrown InvalidSyntaxException
    }

    def "apiParamsAsSelectQuery - null dimensions"() {
        when:
        QueryBuilder.apiParamsAsSelectQuery([null, null], Optional.ofNullable("[eq](datasource)"), Optional.ofNullable("[eq](campaign)"), Optional.ofNullable("[eq](daily)"))

        then:
        thrown InvalidSyntaxException
    }

    def "apiParamsAsSelectQuery - empty-string dimensions"() {
        when:
        QueryBuilder.apiParamsAsSelectQuery([""], Optional.ofNullable("[eq](datasource)"), Optional.ofNullable("[eq](campaign)"), Optional.ofNullable("[eq](daily)"))

        then:
        thrown InvalidSyntaxException
    }

    def "apiParamsAsSelectQuery - null and empty-string dimensions"() {
        when:
        QueryBuilder.apiParamsAsSelectQuery(["", null], Optional.ofNullable("[eq](datasource)"), Optional.ofNullable("[eq](campaign)"), Optional.ofNullable("[eq](daily)"))

        then:
        thrown InvalidSyntaxException
    }
}
