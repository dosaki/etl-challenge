#!/bin/sh

java -cp /etl/etl.jar net.dosaki.etl.RequestHandler "seed"
java -cp /etl/etl.jar net.dosaki.etl.RequestHandler "${ETL_URL}"
java -jar /etl/api.jar