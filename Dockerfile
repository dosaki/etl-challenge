FROM alpine

LABEL org.opencontainers.image.authors="tiago.f.a.correia@gmail.com"

RUN apk --no-cache add openjdk11
RUN mkdir /etl/
RUN echo '0 * * * * java -cp /etl/etl.jar net.dosaki.etl.RequestHandler "\${ETL_URL}" >> /etl/etl.log' > /crontab.txt
RUN crontab /crontab.txt
COPY ./entrypoint.sh /etl/entrypoint.sh
COPY ./etl/build/libs/etl-0.1-all.jar /etl/etl.jar
COPY ./etl-query/build/libs/etl-query-0.1-all.jar /etl/api.jar
RUN crond

WORKDIR /etl/

ENTRYPOINT [ "/etl/entrypoint.sh" ]