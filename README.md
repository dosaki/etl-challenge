# ETL Challenge

This is a tool to extract information from a provided CSV, dump it into a DB and then query it.

## Running

You can check the README files of both projects ([here](./etl/README.md) and [here](./etl-query/README.md)) or you can run this in container mode by using the provided [docker-compose.yml](./docker-compose.yml):
```shell
docker-compose up -d
```
After this, you should be able to access, for example: http://localhost:8888/query?dimension=datasource&dimension=campaign

You can check the rest of the API documentation [here](https://app.swaggerhub.com/apis-docs/dosaki/clickquery).

## Diagram

![diagram](./arch.png)
